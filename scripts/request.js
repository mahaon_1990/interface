const axios = require("./axios.js").axios;



const log_out = () => {
  return new Promise((resolve) => {
    axios.post("users/logout/")
      .then((response) => {
        resolve({ status: response.status });
      })
      .catch((result) => {
        resolve({
          status: result.response.status,
        });
      });
  });
};

const getChoices = () => {
  return new Promise((resolve) => {
    axios.get("operator_choice/")
      .then((response) => {
        resolve({ choices: response.data.results });
      })
      .catch((result) => {
        resolve({
          status: result.response.status,
        });
      });
  });
};

exports.log_out = log_out;
exports.getChoices = getChoices;