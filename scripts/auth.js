const api = require("./token.js");
const utils = require("./utils.js")


const ipc = require("electron").ipcRenderer;

async function auth(credentials, password_value) {
  const response = await api.getToken(credentials);
  if (response.status == 200) {
    utils.setItem('token', response.data)
    utils.setItem('username', credentials.username)
    ipc.send("auth-succses", credentials.username);
  } else {
    const valid = document.querySelector(".valid")
    valid.innerHTML = ''
    response.data.forEach((element) => {
      const error = document.createElement("p");
      error.innerHTML = element;
      valid.append(error);
    });
  }
}

window.addEventListener("DOMContentLoaded", () => {
  document.querySelector("#auth").addEventListener("submit", (e) => {
    e.preventDefault()
    const username = document.getElementById("username").value;
    const password = document.getElementById("password").value;
    auth({ username, password });
  });
});
