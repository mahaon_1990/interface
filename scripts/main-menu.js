const ipc = require("electron").ipcRenderer;
const utils = require("./utils.js");
const request = require("./request.js");

async function log_out() {
  const response = await request.log_out();
  if (response.status == 200) {
    ipc.send("log-out");
  }
}

let lastChoiseId = 0;

async function getChoices() {
  let divNoCommands = document.querySelector(".no_commands");
  let tabl = document.querySelector(".tabl");

  const data = await request.getChoices();
  let choices = data.choices;
  if (choices.length == 0) {
    divNoCommands.classList.add("visible");
  } else {
    tabl.classList.add("visible");
    let table = document.querySelector("table");
    table.innerHTML = "";
    let trHat = document.createElement("tr");
    trHat.innerHTML = `<tr>
                        <th>#</th>
                        <th>Time</th>
                        <th>Command</th>
                        <th>Choice</th>
                        <th>User</th>
                        <th>sa</th>
                      </tr>`;
    table.appendChild(trHat);
    choices.forEach((element) => {
      console.log(element)
      let trElement = document.createElement("tr");
      trElement.innerHTML = `<td>${element.id}</td>
                              <td>${element.date_time.toString().slice(0, 10)} ${element.date_time.toString().slice(11, 19)}</td>
                              <td>${element.command_type}</td>
                              <td>${element.choice}</td>
                              <td>${element.user}</td>
                              <td>${element.node}</td>`;
      table.appendChild(trElement);
    });
  }
}

window.addEventListener("DOMContentLoaded", () => {
  setInterval(getChoices, 10000);

  ipc.on("username", (event) => {
    const username = utils.getItem("username");
    let user = document.querySelector(".user");
    user.innerHTML = username;
  });

  document.querySelector(".log-out").addEventListener("click", (e) => {
    log_out();
  });
});
