const axios = require('axios').default;
const vars = require("./vars.js");

axios.defaults.baseURL = vars.urlHttp
const getToken = (credentials) => {
    return new Promise(resolve => {
        axios.post('users/auth-token/', credentials).then(response => {
            resolve({data: response.data['token'], status: response.status})
          })
          .catch(result => {
            resolve({data: result.response.data.non_field_errors, status: result.response.status})
          })
    }
    )
    }

exports.getToken = getToken
