const getItem = (key) => {
    try {
      return JSON.parse(localStorage.getItem(key));
    } catch (e) {
      console.log("Error getting data from localStorage", e);
      return null;
    }
  };
  
const setItem = (key, data) => {
    try {
      localStorage.setItem(key, JSON.stringify(data));
    } catch (e) {
      console.log("Error saving data in  localStorange", e);
    }
  };

exports.getItem = getItem
exports.setItem = setItem