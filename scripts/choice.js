const ipc = require('electron').ipcRenderer


window.addEventListener('DOMContentLoaded', () => {
    let timerId = null
    let timer = document.querySelector('#seconds')
    const agree = document.querySelector('#accept')
    const drop = document.querySelector('#drop')

    agree.addEventListener('click', () => {
        ipc.send("agree")
        clearInterval(timerId)
    })

    drop.addEventListener('click', () => {
        ipc.send("drop")
        clearInterval(timerId)
    })

    timer.innerHTML = 10

    ipc.on("start-timer", (event) => {
        let seconds = 9
        timerId = setInterval(() => {
            timer.innerHTML = (seconds--).toString()
            if (seconds == -1) {
                ipc.send("drop")
                clearInterval(timerId)
                timer.innerHTML = 10
            }
        }, 1000)
    
        setTimeout(() => { clearInterval(timerId); timer.innerHTML = 10}, 11000);
      });
    
    
})



