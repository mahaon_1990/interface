const axios = require('axios').default;
const utils = require("./utils.js")
const vars = require("./vars.js");

axios.defaults.baseURL = vars.urlHttp
axios.interceptors.request.use(config => {
    const token = utils.getItem('token')
    const authorizationToken = token ? `Token ${token}` : ''
    config.headers.Authorization = authorizationToken
    return config
})


exports.axios = axios