const path = require("path");
const electron = require("electron");
const vars = require("./scripts/vars.js");
var WebSocketClient = require("websocket").client;
const utils = require("./scripts/utils")

const Tray = electron.Tray;
const Menu = electron.Menu;
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

const ipc = electron.ipcMain;

let choiceWindow = null;
let authWindow = null;
let mainMenuWindow = null;
let auth = false;
let isQuiting;
let username = null

function mainMenuWindowCreate() {
  mainMenuWindow = new BrowserWindow({
    width: 600,
    height: 400,
    transparent: true,
    show: false,
    resizable: false,
    icon: path.join(__dirname, "logo.png"),
    webPreferences: {
      nodeIntegration: true,
      preload: path.join(__dirname, "scripts/main-menu.js"),
    },
  });

  // mainMenuWindow.webContents.openDevTools();
  mainMenuWindow.removeMenu();
  mainMenuWindow.loadFile("pages/main-menu.html");
  mainMenuWindow.once("ready-to-show", () => {
    mainMenuWindow.hide();
  });
  mainMenuWindow.on("close", function (event) {
    if (!isQuiting) {
      event.preventDefault();
      mainMenuWindow.hide();
      event.returnValue = false;
    }
  });
}

function authWindowCreate() {
  authWindow = new BrowserWindow({
    width: 600,
    height: 350,
    transparent: true,
    show: false,
    resizable: false,
    webPreferences: {
      nodeIntegration: true,
      preload: path.join(__dirname, "scripts/auth.js"),
    },
  });

  authWindow.webContents.openDevTools();
  authWindow.removeMenu();
  authWindow.loadFile("pages/auth.html");
  authWindow.once("ready-to-show", () => {
    authWindow.show();
  });
  authWindow.on("close", function (event) {
    if (!isQuiting) {
      event.preventDefault();
      authWindow.hide();
      event.returnValue = false;
    }
  });
}

function choiceWindowCreate() {
  choiceWindow = new BrowserWindow({
    width: 1100,
    height: 240,
    transparent: true,
    show: false,
    resizable: false,
    webPreferences: {
      nodeIntegration: true,
      preload: path.join(__dirname, "scripts/choice.js"),
    },
  });

  // choiceWindow.webContents.openDevTools();
  choiceWindow.removeMenu();
  choiceWindow.loadFile("pages/choice.html");
  choiceWindow.once("ready-to-show", () => {
    choiceWindow.hide(); //set hide
  });

  choiceWindow.on("close", function (event) {
    if (!isQuiting) {
      event.preventDefault();
      choiceWindow.hide();
      event.returnValue = false;
    }
  });
}


function createWindow() {
  authWindowCreate();
  mainMenuWindowCreate();
  choiceWindowCreate();
}

app.on("before-quit", function () {
  isQuiting = true;
});

app.on("ready", () => {
  tray = new Tray(path.join(__dirname, "logo.png"));
  tray.setContextMenu(
    Menu.buildFromTemplate([
      {
        label: "Open operator-arkan",
        click: function () {
          if (auth) {
            mainMenuWindow.show();
          } else authWindow.show();
        },
      },
      {
        label: "Quit",
        click: function () {
          isQuiting = true;
          app.quit();
        },
      },
    ])
  );
  createWindow();
});

ipc.on("auth-succses", (event, usrname) => {
  authWindow.hide();
  auth = true;
  mainMenuWindow.webContents.send("username");
  mainMenuWindow.show();
  username = usrname

});

ipc.on("log-out", (event) => {
  mainMenuWindow.hide();
  auth = false;
  authWindow.show();
});




let clientWS = new WebSocketClient();
wsConnect();
clientWS.connect(vars.url, "echo-protocol");

function wsConnect() {
  let first_seq = null
  let message_oper = {}
  let last_choice = ''
  clientWS.on("connectFailed", function (error) {
    console.log("Connect Error: " + error.toString());
  });

  clientWS.on("connect", function (connection) {
    console.log("WebSocket Client Connected");
    connection.on("error", function (error) {
      console.log("Connection Error: " + error.toString());
    });
    connection.on("close", function () {
      console.log("echo-protocol Connection Closed");
    });
    connection.on("message", function (message) {
      if (message.type === "utf8") {
        if (message.utf8Data != "--heartbeat--") {
          msg_json = JSON.parse(message.utf8Data);
          if (msg_json.from == "sa") {
            message_oper = msg_json
            message_oper.user = username
            if ((first_seq == message_oper['seq']) || ('83d18d1f' == message_oper['seq'])) {
              message_oper.from = 'oper'
              message_oper.choice = last_choice
              connection.sendUTF(JSON.stringify(message_oper));
            }
            else{
              choiceWindow.webContents.send("start-timer");
              choiceWindow.show();
              first_seq = message_oper['seq']
              last_choice = ''
            }
           
          }
        }
      }
    });

    ipc.on("agree", (event) => {
      message_oper.from = 'oper'
      message_oper.choice = 'A'
      last_choice = 'A'
      connection.sendUTF(JSON.stringify(message_oper));
      choiceWindow.hide();
    });

    ipc.on("drop", (event) => {
      message_oper.from = 'oper'
      message_oper.choice = 'D'
      last_choice = 'D'
      connection.sendUTF(JSON.stringify(message_oper));
      choiceWindow.hide();
    });

  });
}